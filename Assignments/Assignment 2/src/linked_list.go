package main

import "fmt"

var bufferedIO = newBufferedIO()

type LLNode struct {
	order *Order
	next  *LLNode
}

func newLLNode(order *Order) *LLNode {
	var node = new(LLNode)
	node.order = order
	node.next = nil
	return node
}

type LinkedList struct {
	buyHead   *LLNode
	sellHead  *LLNode
	orderChan chan *Order
	doneChan  chan struct{}
}

func newLinkedList() *LinkedList {
	var ll = new(LinkedList)
	ll.buyHead = newLLNode(nil)
	ll.sellHead = newLLNode(nil)
	ll.orderChan = make(chan *Order, 10000)
	ll.doneChan = make(chan struct{})

	go func() {
		for {
			select {
			case order := <-ll.orderChan:
				switch order.orderType {
				case inputCancel:
					cancel(order, ll.buyHead, ll.sellHead)
					break
				case inputBuy:
					buy(order, ll.sellHead)
					if order.count > 0 {
						insertBuy(order, ll.buyHead)
					}
					break
				case inputSell:
					sell(order, ll.buyHead)
					if order.count > 0 {
						insertSell(order, ll.sellHead)
					}
					break
				}
			case <-ll.doneChan:
				return
			}
		}
	}()

	return ll
}

func (ll *LinkedList) handleOrder(order *Order) {
	ll.orderChan <- order
}

func cancel(cancelOrder *Order, buyHead *LLNode, sellHead *LLNode) {
	var prev = buyHead
	for prev.next != nil {
		var curr = prev.next
		var next = curr.next
		var restingOrder = curr.order

		if canCancel(cancelOrder, restingOrder) {
			prev.next = next
			printOrderDeleted(cancelOrder, true)
			return
		}

		prev = prev.next
	}

	prev = sellHead
	for prev.next != nil {
		var curr = prev.next
		var next = curr.next
		var restingOrder = curr.order

		if canCancel(cancelOrder, restingOrder) {
			prev.next = next
			printOrderDeleted(cancelOrder, true)
			return
		}

		prev = prev.next
	}

	printOrderDeleted(cancelOrder, false)
	return
}

func buy(buyOrder *Order, sellHead *LLNode) {
	var prev = sellHead
	for prev.next != nil && buyOrder.count > 0 {
		var curr = prev.next
		var next = curr.next
		var sellOrder = curr.order

		if canTrade(buyOrder, sellOrder) {
			var tradeVolume = min(buyOrder.count, sellOrder.count)
			buyOrder.count -= tradeVolume
			sellOrder.count -= tradeVolume
			sellOrder.execId += 1
			printOrderExecuted(sellOrder.orderId, buyOrder.orderId, sellOrder.execId, sellOrder.price, tradeVolume, buyOrder.timestamp, GetCurrentTimestamp())
		}

		// If the resting order is complete, delete the node.
		if sellOrder.count == 0 {
			prev.next = next
			continue
		}

		prev = prev.next

		// Early exit.
		if buyOrder.price < sellOrder.price && buyOrder.instrument == sellOrder.instrument {
			return
		}
	}
}

func sell(sellOrder *Order, buyHead *LLNode) {
	var prev = buyHead
	for prev.next != nil && sellOrder.count > 0 {
		var curr = prev.next
		var next = curr.next
		var buyOrder = curr.order

		if canTrade(buyOrder, sellOrder) {
			var tradeVolume = min(buyOrder.count, sellOrder.count)
			sellOrder.count -= tradeVolume
			buyOrder.count -= tradeVolume
			buyOrder.execId += 1
			printOrderExecuted(buyOrder.orderId, sellOrder.orderId, buyOrder.execId, buyOrder.price, tradeVolume, sellOrder.timestamp, GetCurrentTimestamp())
		}

		// If the resting order is complete, delete the node.
		if buyOrder.count == 0 {
			prev.next = next
			continue
		}

		prev = prev.next

		// Early exit.
		if buyOrder.price < sellOrder.price && buyOrder.instrument == sellOrder.instrument {
			return
		}
	}
}

func insertBuy(buyOrder *Order, buyHead *LLNode) {
	var prev = buyHead
	for prev.next != nil {
		var curr = prev.next
		var restingOrder = curr.order

		if canInsertBuy(buyOrder, restingOrder) {
			var node = newLLNode(buyOrder)
			prev.next = node
			node.next = curr
			printOrderAdded(buyOrder)
			return
		}

		prev = prev.next
	}

	prev.next = newLLNode(buyOrder)
	printOrderAdded(buyOrder)
}

func insertSell(sellOrder *Order, buyHead *LLNode) {
	var prev = buyHead
	for prev.next != nil {
		var curr = prev.next
		var restingOrder = curr.order

		if canInsertSell(sellOrder, restingOrder) {
			var node = newLLNode(sellOrder)
			prev.next = node
			node.next = curr
			printOrderAdded(sellOrder)
			return
		}

		prev = prev.next
	}

	prev.next = newLLNode(sellOrder)
	printOrderAdded(sellOrder)
}

func printOrderDeleted(order *Order, accepted bool) {
	acceptedTxt := "A"
	if !accepted {
		acceptedTxt = "R"
	}
	printBuffered(bufferedIO, fmt.Sprintf("X %v %v %v %v\n", order.orderId, acceptedTxt, order.timestamp, GetCurrentTimestamp()))
}

func printOrderAdded(order *Order) {
	orderType := "S"
	if order.orderType == inputBuy {
		orderType = "B"
	}
	printBuffered(bufferedIO, fmt.Sprintf("%v %v %v %v %v %v %v\n", orderType, order.orderId, order.instrument, order.price, order.count, order.timestamp, GetCurrentTimestamp()))
}

func printOrderExecuted(restingId, newId, execId, price, count uint32, inTime, outTime int64) {
	printBuffered(bufferedIO, fmt.Sprintf("E %v %v %v %v %v %v %v\n", restingId, newId, execId, price, count, inTime, outTime))
}
