package main

func min(a uint32, b uint32) uint32 {
	if a < b {
		return a
	}
	return b
}

func max(a uint32, b uint32) uint32 {
	if b < a {
		return a
	}
	return b
}
