package main

type Order struct {
	orderType  inputType
	orderId    uint32
	price      uint32
	count      uint32
	instrument string
	execId     uint32
	timestamp  int64
}

func newOrder(in input) *Order {
	var order *Order = new(Order)
	order.orderType = in.orderType
	order.orderId = in.orderId
	order.price = in.price
	order.count = in.count
	order.instrument = in.instrument
	order.execId = 0
	order.timestamp = GetCurrentTimestamp()

	return order
}

func canTrade(buyOrder *Order, sellOrder *Order) bool {
	return buyOrder.orderType == inputBuy &&
		sellOrder.orderType == inputSell &&
		buyOrder.instrument == sellOrder.instrument &&
		buyOrder.price >= sellOrder.price
}

func canCancel(cancelOrder *Order, restingOrder *Order) bool {
	return cancelOrder.orderType == inputCancel && cancelOrder.orderId == restingOrder.orderId
}

func canInsertBuy(buyOrder *Order, restingOrder *Order) bool {
	return buyOrder.orderType == inputBuy &&
		restingOrder.orderType == inputBuy &&
		buyOrder.instrument == restingOrder.instrument &&
		buyOrder.price > restingOrder.price
}

func canInsertSell(sellOrder *Order, restingOrder *Order) bool {
	return sellOrder.orderType == inputSell &&
		restingOrder.orderType == inputSell &&
		sellOrder.instrument == restingOrder.instrument &&
		sellOrder.price < restingOrder.price
}
