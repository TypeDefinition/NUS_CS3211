package main

import "C"
import (
	"context"
	"fmt"
	"io"
	"net"
	"os"
	"time"
)

type Engine struct{}

var inst_trie = newTrie()
var id_trie = newTrie()

func (e *Engine) accept(ctx context.Context, conn net.Conn) {
	go func() {
		<-ctx.Done()
		conn.Close()
	}()
	go handleConn(conn)
}

func handleConn(conn net.Conn) {
	defer conn.Close()
	for {
		in, err := readInput(conn)
		if err != nil {
			if err != io.EOF {
				_, _ = fmt.Fprintf(os.Stderr, "Error reading input: %v\n", err)
			}
			return
		}

		order := newOrder(in)
		idString := fmt.Sprintf("%v", in.orderId)

		var ll *LinkedList = nil
		if order.orderType == inputCancel {
			ll = id_trie.get(idString)
		} else {
			ll = inst_trie.add(order.instrument)
			id_trie.set(idString, ll)
		}

		ll.handleOrder(order)
	}
}

func GetCurrentTimestamp() int64 {
	return time.Now().UnixMicro()
}
