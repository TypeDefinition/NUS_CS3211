package main

import (
	"strconv"
)

type TrieRequestType int

const (
	REQUEST_SET TrieRequestType = iota
	REQUEST_GET
	REQUEST_ADD
)

type TrieRequest struct {
	requestType    TrieRequestType
	key            string
	linkedListChan chan (*LinkedList)
}

func newTrieRequest(requestType TrieRequestType, key string) *TrieRequest {
	request := new(TrieRequest)
	request.requestType = requestType
	request.key = key
	request.linkedListChan = make(chan *LinkedList)
	return request
}

//Node reprsesents each node in the trie
type TrieNode struct {
	children    [26]*TrieNode
	linkedList  *LinkedList
	requestChan chan (*TrieRequest)
}

func newTrieNode() *TrieNode {
	node := new(TrieNode)
	node.requestChan = make(chan *TrieRequest, 1000)

	go func() {
		for {
			// Receive request.
			request := <-node.requestChan

			// This is the last node. There is no need to traverse anymore.
			if len(request.key) == 0 {
				switch request.requestType {
				case REQUEST_SET:
					node.linkedList = <-request.linkedListChan
					break
				case REQUEST_GET:
					request.linkedListChan <- node.linkedList
					break
				case REQUEST_ADD:
					if node.linkedList == nil {
						node.linkedList = newLinkedList()
					}
					request.linkedListChan <- node.linkedList
					break
				}

				continue
			}

			// Get the child index depending on if the key is a number or a word.
			childIndex := request.key[0] - 'A'
			if _, err := strconv.Atoi(request.key); err == nil {
				childIndex = request.key[0] - '0'
			}

			// Get the child.
			if node.children[childIndex] == nil {
				node.children[childIndex] = newTrieNode()
			}

			// Pass the request to the child.
			request.key = request.key[1:]
			node.children[childIndex].requestChan <- request
		}

	}()

	return node
}

type Trie struct {
	root *TrieNode
}

func newTrie() *Trie {
	return &Trie{root: newTrieNode()}
}

func (t *Trie) set(key string, ll *LinkedList) {
	request := newTrieRequest(REQUEST_SET, key)
	t.root.requestChan <- request
	// This must come AFTER sending the request,
	// as request.linkedListChan is unbuffered.
	// So a send only unblocks if there is a receiver waiting.
	request.linkedListChan <- ll
}

func (t *Trie) add(key string) *LinkedList {
	request := newTrieRequest(REQUEST_ADD, key)
	t.root.requestChan <- request
	return <-request.linkedListChan
}

func (t *Trie) get(key string) *LinkedList {
	request := newTrieRequest(REQUEST_GET, key)
	t.root.requestChan <- request
	return <-request.linkedListChan
}
