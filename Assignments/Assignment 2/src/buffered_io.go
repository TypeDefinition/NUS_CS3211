package main

import "fmt"

type BufferedIO struct {
	stringChan chan string
	doneChan   chan struct{}
}

func newBufferedIO() *BufferedIO {
	var bufferedIO = new(BufferedIO)
	bufferedIO.stringChan = make(chan string, 1000)
	bufferedIO.doneChan = make(chan struct{})

	go func() {
		for {
			select {
			case <-bufferedIO.doneChan:
				break
			case str := <-bufferedIO.stringChan:
				fmt.Print(str)
			}
		}
	}()

	return bufferedIO
}

func printBuffered(bufferedIO *BufferedIO, str string) {
	bufferedIO.stringChan <- str
}

func closeBufferedIO(bufferedIO *BufferedIO) {
	bufferedIO.doneChan <- struct{}{}
}
