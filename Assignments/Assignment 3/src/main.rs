use std::{collections::{HashMap, VecDeque}, thread, time::Instant};
use task::{Task, TaskType};
use std::sync::Mutex;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::SeqCst;
use std::sync::Arc;
use std::sync::mpsc::{channel, Sender};

fn main() {
    let (seed, starting_height, max_children) = get_args();

    eprintln!(
        "Using seed {}, starting height {}, max. children {}",
        seed, starting_height, max_children
    );

    let num_workers = if num_cpus::get() > 0 { num_cpus::get() } else { 1 };
    let generated_tasks = VecDeque::from(Task::generate_initial(seed, starting_height, max_children));
    let num_tasks_left = Arc::new(AtomicUsize::new(generated_tasks.len()));
    let task_pool = Arc::new(Mutex::new(generated_tasks));
    let mut threads = vec![];
    let (tx, rx) = channel();

    // Results
    let mut output: u64 = 0;
    let mut count_map: HashMap<TaskType, usize> = HashMap::new();

    let start = Instant::now();
    // Spawn threads.
    for _ in 0..num_workers {
        let task_pool_clone = task_pool.clone();
        let num_tasks_left_clone = num_tasks_left.clone();
        let tx_clone = tx.clone();
        threads.push(thread::spawn(move || worker_thread(task_pool_clone, num_tasks_left_clone, tx_clone)));
    }
    // Receive results via message passing. (Improvement: No longer have to wait for threads to finish in order. Can start collecting results once any thread is done.)
    for _ in 0..num_workers {
        let result = rx.recv().unwrap();
        output ^= result.0;
        for iter in result.1 {
            *count_map.entry(iter.0).or_insert(0usize) += iter.1;
        }
    }
    // Join threads.
    for t in threads {
        t.join().unwrap();
    }
    let end = Instant::now();

    eprintln!("Completed in {} s", (end - start).as_secs_f64());

    // Print result.
    println!(
        "{},{},{},{}",
        output,
        count_map.get(&TaskType::Hash).unwrap_or(&0),
        count_map.get(&TaskType::Derive).unwrap_or(&0),
        count_map.get(&TaskType::Random).unwrap_or(&0)
    );
}

fn worker_thread(task_pool: Arc<Mutex<VecDeque<Task>>>, num_tasks_left: Arc<AtomicUsize>, tx: Sender<(u64, HashMap<TaskType, usize>)>) {
    let mut output: u64 = 0;
    let mut count_map = HashMap::new();

    while num_tasks_left.load(SeqCst) > 0 {
        let opt_task = { task_pool.lock().unwrap().pop_front() };
        if opt_task != None {
            // Get task.
            let task = opt_task.unwrap();
            *count_map.entry(task.typ).or_insert(0usize) += 1;

            // Execute task.
            let result = task.execute();
            output ^= result.0;

            // Update the number of tasks left. (Must add before subtracting.)
            num_tasks_left.fetch_add(result.1.len(), SeqCst);
            num_tasks_left.fetch_sub(1, SeqCst);

            // Add new tasks into task pool.
            task_pool.lock().unwrap().extend(result.1.into_iter());
        } else {
            thread::yield_now();
        }
    }

    tx.send((output, count_map)).unwrap();
}

// There should be no need to modify anything below
fn get_args() -> (u64, usize, usize) {
    let mut args = std::env::args().skip(1);
    (
        args.next()
            .map(|a| a.parse().expect("invalid u64 for seed"))
            .unwrap_or_else(|| rand::Rng::gen(&mut rand::thread_rng())),
        args.next()
            .map(|a| a.parse().expect("invalid usize for starting_height"))
            .unwrap_or(5),
        args.next()
            .map(|a| a.parse().expect("invalid u64 for seed"))
            .unwrap_or(5),
    )
}

mod task;
