use std::{collections::{HashMap, VecDeque}, thread, time::Instant};
use std::sync::Mutex;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::SeqCst;
use std::sync::Arc;
use std::sync::mpsc::{channel, Sender};
use task::{Task, TaskType, Gen, TYPE_ARRAY};
use rand::{Rng};

fn main() {
    let (seed, starting_height, max_children) = get_args();

    eprintln!(
        "Using seed {}, starting height {}, max. children {}",
        seed, starting_height, max_children
    );

    let num_workers = if num_cpus::get() > 0 { num_cpus::get() } else { 1 };
    let gen_pool: Arc<Mutex<VecDeque<Gen>>> = Arc::new(Mutex::new(VecDeque::new()));
    let num_gen_left = Arc::new(AtomicUsize::new(0));
    let generated_tasks = VecDeque::from(Task::generate_initial(seed, starting_height, max_children));
    let num_tasks_left = Arc::new(AtomicUsize::new(generated_tasks.len()));
    let task_pool = Arc::new(Mutex::new(generated_tasks));
    let mut threads = vec![];
    let (tx, rx) = channel();

    // Results
    let mut output: u64 = 0;
    let mut count_map: HashMap<TaskType, usize> = HashMap::new();

    let start = Instant::now();
    // Spawn threads.
    for _ in 0..num_workers {
        let gen_pool_clone = gen_pool.clone();
        let num_gens_left_clone = num_gen_left.clone();
        let task_pool_clone = task_pool.clone();
        let num_tasks_left_clone = num_tasks_left.clone();
        let tx_clone = tx.clone();
        threads.push(thread::spawn(move || worker_thread(gen_pool_clone, num_gens_left_clone, task_pool_clone, num_tasks_left_clone, tx_clone)));
    }
    // Receive results via message passing. (Improvement: No longer have to wait for threads to finish in order. Can start collecting results once any thread is done.)
    for _ in 0..num_workers {
        let result = rx.recv().unwrap();
        output ^= result.0;
        for iter in result.1 {
            *count_map.entry(iter.0).or_insert(0usize) += iter.1;
        }
    }
    // Join threads.
    for t in threads {
        t.join().unwrap();
    }
    let end = Instant::now();

    eprintln!("Completed in {} s", (end - start).as_secs_f64());

    // Print result.
    println!(
        "{},{},{},{}",
        output,
        count_map.get(&TaskType::Hash).unwrap_or(&0),
        count_map.get(&TaskType::Derive).unwrap_or(&0),
        count_map.get(&TaskType::Random).unwrap_or(&0)
    );
}

fn worker_thread(gen_pool: Arc<Mutex<VecDeque<Gen>>>, num_gens_left: Arc<AtomicUsize>, task_pool: Arc<Mutex<VecDeque<Task>>>, num_tasks_left: Arc<AtomicUsize>, tx: Sender<(u64, HashMap<TaskType, usize>)>) {
    let mut output: u64 = 0;
    let mut count_map = HashMap::new();

    while num_tasks_left.load(SeqCst) > 0 {
        let opt_task = { task_pool.lock().unwrap().pop_front() };
        if opt_task != None {
            // Get task.
            let task = opt_task.unwrap();
            *count_map.entry(task.typ).or_insert(0usize) += 1;

            // Execute task.
            let result = task.execute();
            output ^= result.0;

            // Update the number of gens left.
            if result.1 != None {
                gen_pool.lock().unwrap().push_back(result.1.unwrap());
                num_gens_left.fetch_add(1, SeqCst);
            }

            // Update the number of tasks left.
            num_tasks_left.fetch_sub(1, SeqCst);
        } else {
            thread::yield_now();
        }
    }

    while num_gens_left.load(SeqCst) > 0 {
        let opt_gen = { gen_pool.lock().unwrap().pop_front() };
        if opt_gen != None {
            let mut gen = opt_gen.unwrap();
            if gen.3 == 0 {
                num_gens_left.fetch_sub(1, SeqCst);
                continue;
            }

            let task = Task {
                typ: TYPE_ARRAY[gen.0.gen_range(0..3)],
                seed: gen.0.gen(),
                height: gen.1,
                max_children: gen.2
            };

            gen.3 -= 1;
            if gen.3 > 0 {
                gen_pool.lock().unwrap().push_front(gen);
            } else {
                num_gens_left.fetch_sub(1, SeqCst);
            }

            *count_map.entry(task.typ).or_insert(0usize) += 1;
            // Execute task.
            let result = task.execute();
            output ^= result.0;

            // If there is a new GenInfo, add it to the VecDeque.
            if result.1 != None {
                gen_pool.lock().unwrap().push_back(result.1.unwrap());
                num_gens_left.fetch_add(1, SeqCst);
            }
        } else {
            thread::yield_now();
        }
    }

    tx.send((output, count_map)).unwrap();
}

// There should be no need to modify anything below
fn get_args() -> (u64, usize, usize) {
    let mut args = std::env::args().skip(1);
    (
        args.next()
            .map(|a| a.parse().expect("invalid u64 for seed"))
            .unwrap_or_else(|| rand::Rng::gen(&mut rand::thread_rng())),
        args.next()
            .map(|a| a.parse().expect("invalid usize for starting_height"))
            .unwrap_or(5),
        args.next()
            .map(|a| a.parse().expect("invalid u64 for seed"))
            .unwrap_or(5),
    )
}

mod task;