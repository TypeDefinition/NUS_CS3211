num=$1

g++ ./scripts/generate_input.cpp -o ./scripts/generate_input
./scripts/generate_input 0 $((${num}*1)) > ./scripts/large_1.input
./scripts/generate_input $((${num}*1)) $((${num}*2)) > ./scripts/large_2.input
./scripts/generate_input $((${num}*2)) $((${num}*3)) > ./scripts/large_3.input
./scripts/generate_input $((${num}*3)) $((${num}*4)) > ./scripts/large_4.input
rm ./scripts/generate_input
