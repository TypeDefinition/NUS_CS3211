#include <iostream>
#include <random>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    int start = atoi(argv[1]);
    int end = atoi(argv[2]);

    string instruments[] = {"GOOG", "NVDA", "AMD", "GME", "BB"};

    int i = start;
    while (i < end) {
        int type = rand()%1000;
        if (type < 400) {
            cout << "B " << i++ << " " << instruments[rand()%(sizeof(instruments)/sizeof(instruments[0]))] << " " << (rand()%100 +1) << " " << (rand()%50 + 1) << endl;
        } else if (type < 800) {
            cout << "S " << i++ << " " << instruments[rand()%(sizeof(instruments)/sizeof(instruments[0]))] << " " << (rand()%100 + 1) << " " << (rand()%50 + 1) << endl;
        } else {
            cout << "C " << (rand()%(i - start) + start) << endl;
        }
    }

    return 0;
}
