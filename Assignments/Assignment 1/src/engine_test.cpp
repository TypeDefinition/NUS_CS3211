#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>      // std::get_time
#include <ctime> 
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
//#include <bits/stdc++.h>



using namespace std;



struct input {
    char type;
    int id;
    string instrument;
    int price;
    int size;
};
struct output {
    //for order added to the list
    char type;
    int id;
    string instrument;
    int size;
    int price;
    int64_t ts_recieved;
    int64_t ts_completed;
    int execution_number = 0;
    //for order that has been completed
    int resting_id;
    int new_id;
    int execution;
    //for cancle
    char cancle;
};

string to_string (output out) {
    string str;
    if (out.type == 'S' || out.type == 'B') {
        str += out.type;
        str += " ";
        str += to_string (out.id);
        str += " ";
        str += out.instrument;
        str += " ";
        str += to_string (out.price);
        str += " ";
        str += to_string (out.size);
        str += " ";
    }else if (out.type == 'X') {
        str += out.type;
        str += " ";
        str += to_string (out.resting_id);
        str += " ";
         str += out.cancle;
        str += " ";
    }
    else {
        str += out.type;
        str += " ";
        str += to_string (out.resting_id);
        str += " ";
        str += to_string (out.new_id);
        str += " ";
        str += to_string (out.execution);
        str += " ";
        str += to_string (out.price);
        str += " ";
        str += to_string (out.size);
        str += " ";

    }
    return str;
}


bool order_exists (vector<output> orders, int id) {
    for  (size_t i = 0; i < orders.size (); i++) {
        if (orders[i].id == id) {
            return true;
        }
    }
    return false;
}

bool can_be_matched (vector<output> orders, output order) {
    for (size_t i = 0; i < orders.size (); i++) {
        if (orders[i].type != order.type) {
            continue;
        }
        if (orders[i].instrument != order.instrument) {
            continue;
        }
        
        if (orders[i].ts_recieved < order.ts_recieved) {
            if (order.type == 'B') {
                if (orders[i].price > order.price) {
                    cout << "Better: " << orders[i].type << " " << to_string (orders[i].id) << endl;
                    return false;
                }
            }
            if (order.type == 'S') {
                if (orders[i].price < order.price) {
                    cout << "Better: " << orders[i].id << " " << to_string (orders[i].id) << endl;
                    return false;
                }
            }
        }
        
    }
    return true;
}

std::vector<string> split_input (std::string line) {
   std::vector<std::string> input;
   std::string delimiter = " ";

   size_t pos = 0;
   std::string token;
   while ((pos = line.find(delimiter)) != std::string::npos) {
      token = line.substr(0, pos);
      input.push_back(token);
      line.erase(0, pos + delimiter.length());
   } 
   input.push_back(line);
   return input;
}

output find_order_in_list (vector<output> orders, int id) {
    output out;
    for (size_t i = 0; i < orders.size (); i++) {
        if (orders[i].id == id) {
            out = orders [i];
            return out ;
        }
    }
    return out ;
}

input find_order_in_input (vector<input> orders, int id) {
    input in;
    for (size_t i = 0; i < orders.size (); i++) {
        if (orders[i].id == id) {
            return orders [i];
        }
    }
    return in;
}

input find_buy_sell_order_in_input (vector<input> orders, int id) {
    input in;
    for (size_t i = 0; i < orders.size (); i++) {
        if (orders[i].type == 'C') {
            continue;
        }
        if (orders[i].id == id) {
            return orders [i];
        }
    }
    return in;
}

int find_order_in_input_index (vector<input> orders, int id) {
    for (size_t i = 0; i < orders.size (); i++) {
        
        if (orders[i].id == id) {
            return i;
        }
    }
    return -1;
}

int find_buy_sell_order_in_input_index (vector<input> orders, int id) {
    for (size_t i = 0; i < orders.size (); i++) {
        if (orders[i].type == 'C') {
            continue;
        }
        if (orders[i].id == id) {
            return i;
        }
    }
    return -1;
}

int find_order_in_list_index (vector<output> orders, int id) {

    for (size_t i = 0; i < orders.size (); i++) {
        if (orders[i].id == id) {
            return i;
        }
    }
    return -1;
}

bool price_and_type (input in, output out) {
    if (in.type == out.type) {
        return false;
    }
    if (in.type == 'B' && out.type == 'S' && in.price >= out.price) {
        return true;
    }
    if (in.type == 'S' && out.type == 'B' && in.price <= out.price) {
        return true;
    }
    return false;
}

bool order_exists_in_input (vector<input> inputs, int id) {
    for (size_t i = 0; i < inputs.size (); i++) {
        if (inputs[i].id == id) {
            return true;
        }
    }
    for (size_t i = 0; i < inputs.size (); i++) {
        cout << to_string (inputs[i].id) << " ";
    }
    cout << endl;
    return false;
}

bool order_sell_buy_exists_in_input (vector<input> inputs, int id) {
    for (size_t i = 0; i < inputs.size (); i++) {
        if (inputs[i].id == 'C') {
            continue;
        }
        if (inputs[i].id == id) {
            return true;
        }
    }
    for (size_t i = 0; i < inputs.size (); i++) {
        cout << to_string (inputs[i].id) << " ";
    }
    cout << endl;
    return false;
}

//found it online :)
bool sort_vector(output i1, output i2)
{
    if (i1.ts_recieved == i2.ts_recieved) {
        return (i1.ts_completed < i2.ts_completed);
    }
    return (i1.ts_recieved < i2.ts_recieved);
}

int main() {

    vector<output> order_list;

    std::string output_file_name;
    std::string input_file_name;
    std::cout << "Please write the full name of the input file: "<< std::endl;
    std::cin >> input_file_name;
    std::cout << "Please write the full name of the output file: "<< std::endl;
    std::cin >> output_file_name;
    std::ifstream output_file;
    std::ifstream input_file;
    output_file.open (output_file_name);
    input_file.open (input_file_name);
    std::string line;

    vector<input> inputs;
    if (input_file.is_open()) {
      while (std::getline(input_file, line)){
          vector<string> input_details = split_input (line);
          if (input_details [0] == "S") {
            input in;
            in.type = 'S';
            in.id = stoi (input_details [1]);
            in.instrument = input_details [2];
            in.price = stoi (input_details[3]);
            in.size = stoi (input_details[4]);
            inputs.push_back (in);
          }
          if (input_details [0] == "B") {
            input in;
            in.type = 'B';
            in.id = stoi (input_details [1]);
            in.instrument = input_details [2];
            in.price = stoi (input_details[3]);
            in.size = stoi (input_details[4]);
            inputs.push_back (in);
          }
          if (input_details [0] == "C") {
            input in;
            in.type = 'C';
            in.id = stoi (input_details [1]);
            inputs.push_back (in);
          }
          
      }
      input_file.close ();
   }
   else {
      std::cout << "Can not open the file";
   }
    
    /*for (int i = 0; i < inputs.size (); i ++) {
        cout << inputs[i].type << " " <<  inputs[i].id  << " " <<  inputs[i].size << endl;
    }*/

    if (output_file.is_open()) {
      while (std::getline(output_file, line)){
          output out;
          vector<string> output_details = split_input (line);
          if (output_details[0] == "B") {
              out.type = 'B';
              out.id = stoi (output_details[1]),
              out.instrument = output_details [2];
              out.price = stoi (output_details[3]);
              out.size = stoi (output_details [4]);
              out.ts_recieved = stoll (output_details [5].c_str());
              out.ts_completed = stoll (output_details [6].c_str());
          }
          if (output_details[0] == "S") {
              out.type = 'S';
              out.id = stoi (output_details[1]),
              out.instrument = output_details [2];
              out.price = stoi (output_details[3]);
              out.size = stoi (output_details [4]);
              out.ts_recieved = stoll (output_details [5].c_str());
              out.ts_completed = stoll (output_details [6].c_str());
              
          }
          if (output_details[0] == "X") {
              out.type = 'X';
              out.id = stoi (output_details[1]),
              out.cancle = output_details [2][0];
              out.ts_recieved = stoll (output_details [3].c_str());
              out.ts_completed = stoll (output_details [4].c_str());
          }
          if (output_details[0] == "E")  {
              out.type = 'E';
              out.resting_id = stoi (output_details[1]);
              out.new_id = stoi (output_details[2]);
              out.execution = stoi (output_details[3]);
              out.price = stoi (output_details[4]);
              out.size = stoi (output_details[5]);
              out.ts_recieved = stoll (output_details [6].c_str());
              out.ts_completed = stoll (output_details [7].c_str());
          }
          order_list.push_back (out);
      }
      output_file.close ();
    }
    else {
      std::cout << "Can not open the file";
    }
    
    
    //sorting the vector by the timestamp
    sort(order_list.begin(), order_list.end(), sort_vector);

    for (size_t i = 0; i < order_list.size (); i++) {
        if (order_list[i].type == 'B' || order_list[i].type == 'S') {
            cout << order_list[i].type << " " << order_list[i].id <<  " " << order_list[i].size << endl;
        }
        //cout << to_string (order_list[i]) << endl;
    }

    vector<output> orders;
    bool correct = true;

    for (size_t i = 0; i < order_list.size (); i++) {
        if (order_list[i].type == 'S' || order_list[i].type == 'B') {
            if (!order_exists_in_input (inputs, order_list[i].id)) {
                cout << "Failed at input: " << order_list[i].type << endl;
                cout << "order_exists_in_input" << endl;
                correct = false;
                break;
            }

            //there are two orders with the same id or there was never that input
            if (order_exists (orders, order_list[i].id)) {
                cout << "Failed at input: " << order_list[i].type << endl;
                cout << "order_exists" << endl;
                cout << "Order id: " << order_list[i].id << endl;
                cout << "Index: " << i << endl;
                correct = false;
                break;
            }

            cout << "We added order: " << to_string (order_list[i]) << endl;
            orders.push_back (order_list[i]);
        }

        if (order_list[i].type == 'X') {
            if (order_list[i].cancle == 'A') {
                if (!order_exists(orders, order_list[i].id)) {
                    cout << "Failed at: " << order_list[i].type << endl;
                    cout << "Error: C" << endl;
                    correct = false;
                    break;
                }

                int index = find_order_in_list_index (orders, order_list[i].id);
                orders.erase (orders.begin () + index);
            }
            
            if (order_list[i].cancle == 'R') {
                if (order_exists(orders, order_list[i].id)) {
                    cout << "Failed at: " << order_list[i].type << endl;
                    cout << "Error: D" << endl;
                    correct = false;
                    break;
                }
            }
        }

        if (order_list[i].type == 'E') {
            output resting_order = find_order_in_list (orders, order_list[i].resting_id);
            input new_order = find_buy_sell_order_in_input (inputs, order_list[i].new_id);
            
            if (!order_exists (orders, order_list[i].resting_id)) {
                cout << "Failed at: " << "order_exists" << endl;
                cout << "Restin id: " << order_list[i].resting_id << endl;
                cout << "New id: " << order_list[i].new_id << endl;
                cout << "Index: " << i << endl;
                correct = false;
                break;
            }

            if (!order_sell_buy_exists_in_input (inputs, order_list[i].new_id)) {
                cout << "Failed at: " << "order_exists_in_input" << endl;
                cout << "Restin id: " << order_list[i].resting_id << endl;
                cout << "New id: " << order_list[i].new_id << endl;
                cout << "Index: " << i << endl;
                correct = false;
                break;
            }

            if (resting_order.size <= 0) {
                cout << "Failed at: " << order_list[i].type << endl;
                cout << "Error: G" << endl;
                correct = false;
                break;
            }

            if (new_order.size <= 0) {
                cout << "Failed at: " << order_list[i].type << endl;
                cout << "size" << endl;
                cout << "new id: " << order_list[i].new_id << endl;
                int input_index = find_order_in_input_index (inputs, order_list[i].new_id);
                cout << "The size of new-input order : " << to_string (inputs[input_index].size) << endl;
                cout << "resting id: " << order_list[i].resting_id << endl;
                cout << "The size of resting-output order : " << to_string (resting_order.size) << endl;
                cout << "index: " << i << endl;
                correct = false;
                break;
            }

            if (!price_and_type(new_order, resting_order)) {
                cout << "Failed at: " << to_string (resting_order) << endl;
                cout << "price_and_type" << endl;
                cout << "new_order: " << new_order.type << " " << new_order.price << endl;
                cout << "resting_order: " << resting_order.type << " " << resting_order.price << endl;
                cout << "index: " << i << endl;
                correct = false;
                break;
            }
            if (!can_be_matched (orders, resting_order)) {
                cout << "Failed at: can_be_matched: " << to_string (resting_order) << endl;
                cout << "new_order: " << new_order.type << " " << new_order.id << endl;
                cout << "resting_order: " << resting_order.type << " " << resting_order.id << endl;
                cout << "index: " << i << endl;
                correct = false;
                break;
            }
            //cahange in the input

            int input_index = find_order_in_input_index (inputs, order_list[i].new_id);
            cout << "new order: " << new_order.type << " " << new_order.id << endl;
            cout << "The size of new-input order : " << to_string (inputs[input_index].size) << endl;
             cout << "resting order: " << resting_order.type << " " << resting_order.id << endl;
            cout << "The size of resting-output order : " << to_string (resting_order.size) << endl;
            inputs[input_index].size -= order_list[i].size;
            cout << "The new size in the input of this oreder is: " << to_string(inputs[input_index].size)  << endl;
            /*if (inputs[input_index].size <= 0) {
                cout << "We erased in input: "  << endl;
                inputs.erase (inputs.begin () + input_index);
            }*/


            //substract the size
            int index = find_order_in_list_index (orders, order_list[i].resting_id);
            cout << "We changed the order: " << to_string (orders[index]) << endl;
            cout << "The size of the new order is: " << to_string (new_order.size) << endl;
            orders[index].size -= order_list[i].size;
            cout << "The new size of the order is: " << to_string(orders[index].size) << endl;
            if (orders[index].size <= 0) {
                cout << "We erased: " << to_string (orders[index]) << endl;
                orders.erase (orders.begin () + index);
            }
        }
    }
    if (correct) {
        cout << "Correct!" << endl;
    } else {
        cout << "Error Detected!" << endl;
    }

    return 0;
}
