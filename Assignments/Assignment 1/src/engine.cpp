#include "engine.hpp"

#include <iostream>
#include <thread>

#include "io.h"
#include <vector>

void Engine::Accept(ClientConnection connection) {
    std::thread thread{&Engine::ConnectionThread, this,
                       std::move(connection)};
    thread.detach();
}

void Engine::ConnectionThread(ClientConnection connection) {
    while (true) {
        input in;
        switch (connection.ReadInput(in)) {
            case ReadResult::Error:
                std::cerr << "Error reading in" << std::endl;
            case ReadResult::EndOfFile:
                return;
            case ReadResult::Success:
                break;
        }
        order new_order{in};
        switch (new_order.type) {
            case input_cancel:
            {
                order_trie.cancel(new_order);
            }
                break;
            case input_buy:
            {
                size_t hash = hasher_(new_order.instrument) % NUM_ORDER_LIST;
                order_trie.buy(new_order, &order_lists_[hash]);
            }
                break;
            case input_sell:
            {
                size_t hash = hasher_(new_order.instrument) % NUM_ORDER_LIST;
                order_trie.sell(new_order, &order_lists_[hash]);
            }
                break;
            default:
                break;
        }
    }
}