#ifndef SAFE_OUTPUT_H
#define SAFE_OUTPUT_H

#include <mutex>

#include "io.h"

using namespace std;

class safe_output {
private:
    static mutex print_mutex_;

public:
    static void order_added(uint32_t _id, const char *_symbol,
                            uint32_t _price, uint32_t _count,
                            bool _is_sell_side,
                            intmax_t _input_timestamp,
                            intmax_t _output_timestamp) {
        lock_guard lock{print_mutex_};
        Output::OrderAdded(_id, _symbol, _price, _count, _is_sell_side, _input_timestamp, _output_timestamp);
    }

    static void order_executed(uint32_t _resting_id, uint32_t _new_id,
                               uint32_t _execution_id, uint32_t _price,
                               uint32_t _count,
                               intmax_t _input_timestamp,
                               intmax_t _output_timestamp) {
        lock_guard lock{print_mutex_};
        Output::OrderExecuted(_resting_id, _new_id, _execution_id, _price, _count, _input_timestamp,
                              _output_timestamp);
    }

    static void order_deleted(uint32_t _id, bool _cancel_accepted,
                              intmax_t _input_timestamp,
                              intmax_t _output_timestamp) {
        lock_guard lock{print_mutex_};
        Output::OrderDeleted(_id, _cancel_accepted, _input_timestamp, _output_timestamp);
    }
};

#endif
