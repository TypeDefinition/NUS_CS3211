#include <string>

#include "safe_output.hpp"
#include "order_trie.hpp"
#include "order_list.hpp"
#include "engine.hpp"

order_trie::node *order_trie::get_child(node *_parent, size_t _index) {
    // If the child does not exist, create it.
    while (!_parent->has_child_[_index]) {
        // There might be multiple threads trying to create the child.
        // The one that succeeds in locking the mutex will create the child.
        unique_lock lock{_parent->children_mutexes_[_index], try_to_lock};
        if (lock.owns_lock()) {
            _parent->children_[_index] = make_unique<node>();
            _parent->has_child_[_index] = true;
        }
    }

    return _parent->children_[_index].get();
}

void order_trie::cancel(order _new) {
    node *curr = &head_;

    string key = to_string(_new.order_id);
    for (char c: key) {
        curr = get_child(curr, c - '0');
    }


    // Read from the trie and release the lock ASAP.
    shared_lock read_lock{curr->shared_mutex_};
    if (curr->order_list_) {
        // Assumption: curr->order_list_ will only ever be written to once.
        // If the code reaches this point, curr->order_list_ has already been written to and will never change ever again.
        read_lock.unlock();
        curr->order_list_->cancel(_new);
    } else {
        // Read the timestamp before unlocking the mutex, to prevent edge case where the
        // buy/sell order arrives and is added to the list before the cancel is able to print.
        int64_t timestamp = CurrentTimestamp();
        read_lock.unlock();
        safe_output::order_deleted(_new.order_id, false, _new.timestamp, timestamp);
    }
}

void order_trie::buy(order _new, order_list *_order_list) {
    node *curr = &head_;

    string key = to_string(_new.order_id);
    for (char c: key) {
        curr = get_child(curr, c - '0');
    }

    // Write to the trie and release the lock ASAP.
    unique_lock write_lock{curr->shared_mutex_};
    curr->order_list_ = _order_list;
    write_lock.unlock();
    _order_list->buy(_new);
}

void order_trie::sell(order _new, order_list *_order_list) {
    node *curr = &head_;

    string key = to_string(_new.order_id);
    for (char c: key) {
        curr = get_child(curr, c - '0');
    }

    // Write to the trie and release the lock ASAP.
    unique_lock write_lock{curr->shared_mutex_};
    curr->order_list_ = _order_list;
    write_lock.unlock();
    _order_list->sell(_new);
}