//
// Created by lnxterry on 15/2/22.
//

#ifndef ORDER_H
#define ORDER_H

#include <cstring>
#include <optional>
#include <memory>
#include <atomic>

#include "io.h"

using namespace std;

/**
 * Basically a wrapper around input, but with more information.
 */
struct order {
    const enum input_type type;
    const uint32_t order_id;
    const uint32_t price;
    uint32_t count;
    const string instrument;

    uint32_t exec_id;
    const int64_t timestamp;

    order();
    order(const input &_input);
};

#endif