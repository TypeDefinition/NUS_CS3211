// This file contains declarations for the main Engine class. You will
// need to add declarations to this file as you develop your Engine.

#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <chrono>

#include "io.h"

#include "order_list.hpp"
#include "order_trie.hpp"

#define NUM_ORDER_LIST 113

class Engine {
private:
    hash<string> hasher_;
    order_trie order_trie;
    order_list order_lists_[NUM_ORDER_LIST];

    void ConnectionThread(ClientConnection);
   
public:
    void Accept(ClientConnection);
};

inline static std::chrono::microseconds::rep CurrentTimestamp() noexcept {
    return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
}

#endif