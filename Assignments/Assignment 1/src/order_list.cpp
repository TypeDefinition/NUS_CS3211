#include <cassert>

#include "safe_output.hpp"
#include "order_list.hpp"
#include "engine.hpp"

bool order_list::can_buy(order &_new, order &_rest) {
    assert(_new.type == input_buy);

    if (_rest.type != input_sell) { return false; }
    if (_new.instrument != _rest.instrument) { return false; }
    if (_new.price < _rest.price) { return false; }
    return true;
}

bool order_list::can_sell(order &_new, order &_rest) {
    assert(_new.type == input_sell);

    if (_rest.type != input_buy) { return false; }
    if (_new.instrument != _rest.instrument) { return false; }
    if (_rest.price < _new.price) { return false; }
    return true;
}

bool order_list::can_insert_buy(order &_new, order &_rest) {
    assert(_new.type == input_buy);

    return (_rest.type == _new.type) && (_new.price > _rest.price) && (_rest.instrument == _new.instrument);
}

bool order_list::can_insert_sell(order &_new, order &_rest) {
    assert(_new.type == input_sell);

    return (_rest.type == _new.type) && (_new.price < _rest.price) && (_rest.instrument == _new.instrument);
}

void order_list::print_buy(const order &_new, const order &_rest, uint32_t _count) {
    safe_output::order_executed(_rest.order_id, _new.order_id, _rest.exec_id, _rest.price, _count, _new.timestamp,
                                CurrentTimestamp());
}

void order_list::print_sell(const order &_new, const order &_rest, uint32_t _count) {
    safe_output::order_executed(_rest.order_id, _new.order_id, _rest.exec_id, _rest.price, _count, _new.timestamp,
                                CurrentTimestamp());
}

void order_list::cancel(const order& _new) {
    lock_guard lock{mutex_};

    for (auto iter = buy_orders_.begin(); iter != buy_orders_.end(); ++iter) {
        if (_new.order_id == iter->order_id) {
            buy_orders_.erase(iter);
            safe_output::order_deleted(_new.order_id, true, _new.timestamp, CurrentTimestamp());
            return;
        }
    }

    for (auto iter = sell_orders_.begin(); iter != sell_orders_.end(); ++iter) {
        if (_new.order_id == iter->order_id) {
            sell_orders_.erase(iter);
            safe_output::order_deleted(_new.order_id, true, _new.timestamp, CurrentTimestamp());
            return;
        }
    }

    safe_output::order_deleted(_new.order_id, false, _new.timestamp, CurrentTimestamp());
}

void order_list::buy(order _new) {
    lock_guard lock{mutex_};

    auto sell_iter = sell_orders_.begin();
    while (sell_iter != sell_orders_.end()) {
        // Execute order.
        if (can_buy(_new, *sell_iter)) {
            int trade_vol = min(_new.count, sell_iter->count);
            _new.count -= trade_vol;
            sell_iter->count -= trade_vol;
            ++sell_iter->exec_id;
            print_buy(_new, *sell_iter, trade_vol);
        }

        // Resting order complete. Delete node.
        if (!sell_iter->count) {
            sell_iter = sell_orders_.erase(sell_iter);
        } else {
            ++sell_iter;
        }

        // New order has completed.
        if (!_new.count) {
            return;
        }
    }

    // New order has completed.
    if (!_new.count) {
        return;
    }

    // New order incomplete and reached the end of the list. Append to list.
    for (auto buy_iter = buy_orders_.begin(); buy_iter != buy_orders_.end(); ++buy_iter) {
        if (can_insert_buy(_new, *buy_iter)) {
            buy_orders_.insert(buy_iter, _new);
            safe_output::order_added(_new.order_id, _new.instrument.c_str(), _new.price, _new.count, _new.type == input_sell,
                                     _new.timestamp, CurrentTimestamp());
            return;
        }
    }
    buy_orders_.push_back(_new);
    safe_output::order_added(_new.order_id, _new.instrument.c_str(), _new.price, _new.count, _new.type == input_sell,
                             _new.timestamp, CurrentTimestamp());
}

void order_list::sell(order _new) {
    lock_guard lock{mutex_};

    auto buy_iter = buy_orders_.begin();
    while (buy_iter != buy_orders_.end()) {
        // Execute order.
        if (can_sell(_new, *buy_iter)) {
            int trade_vol = min(_new.count, buy_iter->count);
            _new.count -= trade_vol;
            buy_iter->count -= trade_vol;
            ++buy_iter->exec_id;
            print_sell(_new, *buy_iter, trade_vol);
        }

        // Resting order complete. Delete node.
        if (!buy_iter->count) {
            buy_iter = buy_orders_.erase(buy_iter);
        } else {
            ++buy_iter;
        }

        // New order has completed.
        if (!_new.count) {
            return;
        }
    }

    // New order has completed.
    if (!_new.count) {
        return;
    }

    // New order incomplete and reached the end of the list. Append to list.
    for (auto sell_iter = sell_orders_.begin(); sell_iter != sell_orders_.end(); ++sell_iter) {
        if (can_insert_sell(_new, *sell_iter)) {
            sell_orders_.insert(sell_iter, _new);
            return;
        }
    }
    sell_orders_.push_back(_new);
    safe_output::order_added(_new.order_id, _new.instrument.c_str(), _new.price, _new.count, _new.type == input_sell,
                             _new.timestamp, CurrentTimestamp());
}
