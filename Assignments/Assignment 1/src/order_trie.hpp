//
// Created by lnxterry on 20/2/22.
//

#ifndef ORDER_TRIE_H
#define ORDER_TRIE_H

#include <atomic>
#include <optional>
#include <mutex>
#include <shared_mutex>
#include <memory>

#include "order.hpp"

using namespace std;

#define NUM_TRIE_NEXT 10

class order_list;

class order_trie {
private:
    struct node {
        order_list *order_list_;
        mutable shared_mutex shared_mutex_;
        atomic_bool has_child_[NUM_TRIE_NEXT] = {false};
        unique_ptr<node> children_[NUM_TRIE_NEXT] = {nullptr};
        mutable mutex children_mutexes_[NUM_TRIE_NEXT];

        explicit node() : order_list_{nullptr} {}
    };

    node head_;

    node *get_child(node *_parent, size_t _index);

public:
    order_trie() = default;

    void cancel(order _new);

    void buy(order _new, order_list *_order_list);

    void sell(order _new, order_list *_order_list);
};


#endif
