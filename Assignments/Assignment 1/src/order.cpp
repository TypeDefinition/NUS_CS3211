//
// Created by lnxterry on 15/2/22.
//

#include "order.hpp"
#include "engine.hpp"

order::order(const input &_input)
        : type(_input.type),
          order_id(_input.order_id),
          price(_input.price),
          count(_input.count),
          instrument(_input.instrument),
          exec_id(0),
          timestamp(CurrentTimestamp()) {
}

order::order()
        : type(input_cancel),
          order_id(0),
          price(0),
          count(0),
          exec_id(0),
          timestamp(0) {
}