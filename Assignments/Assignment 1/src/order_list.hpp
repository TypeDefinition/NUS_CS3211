//
// Created by lnxterry on 13/2/22.
//

#ifndef ORDER_LIST_H
#define ORDER_LIST_H

#include <mutex>
#include <shared_mutex>
#include <memory>
#include <functional>
#include <list>
#include <queue>

#include "order.hpp"

using namespace std;

class order_list {
private:
    mutex mutex_;
    list<order> buy_orders_;
    list<order> sell_orders_;

    static bool can_buy(order& _new, order& _rest);
    static bool can_sell(order& _new, order& _rest);
    static bool can_insert_buy(order& _new, order& _rest);
    static bool can_insert_sell(order& _new, order& _rest);
    static void print_buy(const order& _new, const order& _rest, uint32_t count);
    static void print_sell(const order& _new, const order& _rest, uint32_t count);

public:
    order_list() = default;

    void buy(order _new);
    void sell(order _new);
    void cancel(const order& _new);
};

#endif